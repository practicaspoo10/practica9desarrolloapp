package ito.poo.clases;

import java.time.LocalDate;

public class Viajes {
	
	private  String ciuDest =  " ";
	private  String dir =  " ";
	private  LocalDate fechaViaje =  null;
	private  LocalDate fechaReg =  null;
	private  String descripCarga =  " ";
	private  String montViaje = " ";

	 public Viajes () {
		super ();
	}
	 public Viajes(String ciuDest, String dir, LocalDate fechaViaje, LocalDate fechaReg,String descripCarga, String montViaje) {
	 super();
		this.ciuDest = ciuDest;
		this.dir = dir;
		this.fechaViaje = fechaViaje;
		this.fechaReg = fechaReg;
		this.descripCarga = descripCarga;
		this.montViaje = montViaje;
	 }
	 
	public float agregarViaje() {
		return 0 ;
	}
		
	public float eliminarViaje() {
		return 0;
	}
	
	public String getCiuDest() {
		return ciuDest;
	}
	public void setCiuDest(String ciuDest) {
		this.ciuDest = ciuDest;
	}
	public String getDir() {
		return dir;
	}
	public void setDir(String dir) {
		this.dir = dir;
	}
	public LocalDate getFechaViaje() {
		return fechaViaje;
	}
	public void setFechaViaje(LocalDate fechaViaje) {
		this.fechaViaje = fechaViaje;
	}
	public LocalDate getFechaReg() {
		return fechaReg;
	}
	public void setFechaReg(LocalDate fechaReg) {
		this.fechaReg = fechaReg;
	}
	
	public String getDescripCarga() {
		return descripCarga;
	}
	public void setDescripCarga(String descripCarga) {
		this.descripCarga = descripCarga;
	}
	public String getMontViaje() {
		return montViaje;
	}
	public void setMontViaje(String montViaje) {
		this.montViaje = montViaje;
	}

	@Override
	public String toString() {
		return "Viajes [ciuDest=" + ciuDest + ", dir=" + dir + ", fechaViaje=" + fechaViaje
				+ ", fechaReg=" + fechaReg + ", descripCarga=" + descripCarga + ", montViaje="
				+ montViaje + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ciuDest == null) ? 0 : ciuDest.hashCode());
		result = prime * result + ((descripCarga == null) ? 0 : descripCarga.hashCode());
		result = prime * result + ((dir == null) ? 0 : dir.hashCode());
		result = prime * result + ((fechaReg == null) ? 0 : fechaReg.hashCode());
		result = prime * result + ((fechaViaje == null) ? 0 : fechaViaje.hashCode());
		result = prime * result + ((montViaje == null) ? 0 : montViaje.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Viajes other = (Viajes) obj;
		if (ciuDest == null) {
			if (other.ciuDest!= null)
				return false;
		} else if (!ciuDest.equals(other.ciuDest))
			return false;
		if (descripCarga == null) {
			if (other.descripCarga != null)
				return false;
		} else if (!descripCarga.equals(other.descripCarga))
			return false;
		if (dir == null) {
			if (other.dir != null)
				return false;
		} else if (!dir.equals(other.dir))
			return false;
		if (fechaReg == null) {
			if (other.fechaReg != null)
				return false;
		} else if (!fechaReg.equals(other.fechaReg))
			return false;
		if (fechaViaje == null) {
			if (other.fechaViaje != null)
				return false;
		} else if (!fechaViaje.equals(other.fechaViaje))
			return false;
		if (montViaje == null) {
			if (other.montViaje != null)
				return false;
		} else if (!montViaje.equals(other.montViaje))
			return false;
		return true;
	}
	public int compareTo(Viajes arg0) {
		int r=0;
		if (!this.ciuDest.equals(arg0.getCiuDest()))
			return this.ciuDest.compareTo(arg0.getCiuDest());
		else if (!this.dir.equals(arg0.getDir()))
			return this.dir.compareTo(arg0.getDir());
		else if (!this.fechaViaje.equals(arg0.getFechaViaje()))
			return this.fechaViaje.compareTo(arg0.getFechaViaje());
		else if (!this.fechaReg.equals(arg0.getFechaReg()))
			return this.fechaReg.compareTo(arg0.getFechaReg());
		else if (!this.descripCarga.equals(arg0.getDescripCarga()))
			return this.descripCarga.compareTo(arg0.getDescripCarga());
		else if (!this.montViaje.equals(arg0.getMontViaje()))
			return this.montViaje.compareTo(arg0.getMontViaje());
		return r;
	}
}

	
		


