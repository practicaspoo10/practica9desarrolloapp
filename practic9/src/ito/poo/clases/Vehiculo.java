package ito.poo.clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class Vehiculo {

	private String regMarca =  " ";
	private String modelo =  " ";
	private float cantMax = 0F;
	private LocalDate fechaAdqui = null;
	private ArrayList<Viajes> viajesRealizados = new ArrayList<Viajes>();
	private int identificador;
	
	public Vehiculo() {
		super();
	}
	
	public Vehiculo(String regMarca, String modelo, float cantMax, LocalDate fechaAdqui,
			ArrayList<Viajes> viajesRealizados, int identificador) {
		super();
		this.regMarca = regMarca;
		this.modelo = modelo;
		this.cantMax = cantMax;
		this.fechaAdqui = fechaAdqui;
		this.viajesRealizados = viajesRealizados;
		this.identificador = identificador;
	}
	
	public  boolean  addViaje (Viajes  newViaje) {
		boolean addViaje = false;
		addViaje = this.viajesRealizados.add(newViaje);
		return addViaje;
	}

	public  boolean  elimViaje ( Viajes  viaje ) {
		boolean elimViaje = false;
		elimViaje = this.viajesRealizados.remove(viaje);
		return elimViaje;
	}

	public String getRegMarca() {
		return regMarca;
	}

	public void setRegMarca(String regMarca) {
		this.regMarca = regMarca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public float getCantMax() {
		return cantMax;
	}

	public void setCantMax(float cantMax) {
		this.cantMax = cantMax;
	}

	public LocalDate getFechaAdqui() {
		return fechaAdqui;
	}

	public void setFechaAdqui(LocalDate fechaAdqui) {
		this.fechaAdqui = fechaAdqui;
	}

	public ArrayList<Viajes> getViajesRealizados() {
		return viajesRealizados;
	}

	public void setViajesRealizados(ArrayList<Viajes> viajesRealizados) {
		this.viajesRealizados = viajesRealizados;
	}

	public int getIdentificador() {
		return identificador;
	}

	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}

	@Override
	public String toString() {
		return "Vehiculo [regMarca=" + regMarca + ", modelo=" + modelo + ", cantMax=" + cantMax
				+ ", fechaAdqui=" + fechaAdqui + ", viajesRealizados=" + viajesRealizados
				+ ", identificador=" + identificador + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(cantMax);
		result = prime * result + ((fechaAdqui == null) ? 0 : fechaAdqui.hashCode());
		result = prime * result + identificador;
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + ((regMarca == null) ? 0 : regMarca.hashCode());
		result = prime * result + ((viajesRealizados == null) ? 0 : viajesRealizados.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehiculo other = (Vehiculo) obj;
		if (Float.floatToIntBits(cantMax) != Float.floatToIntBits(other.cantMax))
			return false;
		if (fechaAdqui == null) {
			if (other.fechaAdqui != null)
				return false;
		} else if (!fechaAdqui.equals(other.fechaAdqui))
			return false;
		if (identificador != other.identificador)
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (regMarca == null) {
			if (other.regMarca != null)
				return false;
		} else if (!regMarca.equals(other.regMarca))
			return false;
		if (viajesRealizados == null) {
			if (other.viajesRealizados != null)
				return false;
		} else if (!viajesRealizados.equals(other.viajesRealizados))
			return false;
		return true;
	}
	
	public int compareTo(Vehiculo arg0) {
		int r=0;
		if (!this.regMarca.equals(arg0.getRegMarca()))
			return this.regMarca.compareTo(arg0.getRegMarca());
		else if (!this.modelo.equals(arg0.getModelo()))
			return this.modelo.compareTo(arg0.getModelo());
		else if(this.cantMax != arg0.getCantMax())
			return this.cantMax>arg0.getCantMax()?1:-1;
		else if (!this.fechaAdqui.equals(arg0.getFechaAdqui()))
			return this.fechaAdqui.compareTo(arg0.getFechaAdqui());
		else if(this.identificador != arg0.getIdentificador())
			return this.identificador>arg0.getIdentificador()?1:-1;
		return r;
	}	
}

	


