package ito.poo.clases;

import ito.dsc.input.FormInput;
import ito.dsc.output.FormOutput;
import java.time.LocalDate;
import java.util.ArrayList;

public class Menu {

		static private ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
		static private ArrayList<Viajes> viajes = new ArrayList<Viajes>();
		static private FormInput menu = new FormInput();
		static private final int FIN = 7;
		/***************************/
		static void generaMenu() {
			menu.addItemMenu("1.- Agrega un Vehiculo");
			menu.addItemMenu("2.- Lista de Vehiculos");
			menu.addItemMenu("3.- Eliminar un Vehiculo");
			menu.addItemMenu("4.- Cancelar el Viaje");
			menu.addItemMenu("5.- Asignar un viaje al Vehiculo");
			menu.addItemMenu("6.- Viajes asignados al Vehiculo");
			menu.addItemMenu("7.- Salir :)");
		}
		/***************************/
		static String capRegMarca() {
			String regmarca;
			String mvregmarca = FormInput.leeString("Ingresa por favor la Marca del Vehiculo");
			regmarca = String.format(mvregmarca);
			return regmarca;
		}

		static String capmodelo() {
			String capmodelo;
			String cmcapmodelo = FormInput.leeString("Ingresa por favor el Modelo");
			capmodelo = String.format(cmcapmodelo);
			return capmodelo;
		}

		static float capcantMax() {
			float cantMax;
			float cmcantMax= FormInput.leeFloat("Ingresa por favor la cantidad Max de la carga");
			cantMax = Float.floatToIntBits(cmcantMax);
			return cantMax;
		}

		static LocalDate capfechaAdqui() {
			LocalDate fechaAdqui;
			String facapfechaAdqui = FormInput.leeString("Ingresa por favor la fecha de la cita: [aaaa-mm-dd]:");
			fechaAdqui = LocalDate.parse(facapfechaAdqui);
			return fechaAdqui;
		}

		static Vehiculo AgregarVehiculo() {
			String regMarca = capRegMarca();
			String modelo = capmodelo();
			float cantMax = capcantMax();
			LocalDate fechaAdqui = capfechaAdqui();
			return new Vehiculo(regMarca, modelo, cantMax, fechaAdqui, viajes, 0);
		}

		static void addVehiculo() {
			Vehiculo vehiculo = AgregarVehiculo();
			vehiculos.add(vehiculo);
		}

		static void listVehiculos() {
			if (!vehiculos.isEmpty())
			FormOutput.imprimeListaTabla(vehiculos, "Lista de citas");
		}

		static void EliminarVehiculo() {
			for (Vehiculo vehiculo : vehiculos)
				if (FormInput.leeBoolean(vehiculo + "Este es el viaje a eliminar:")) {
					vehiculos.remove(vehiculo);
					break;
				}
		}
		/***************************/
		static String capciuDest() {
			String ciuDest;
			String cdciuDest = FormInput.leeString("Ciudad de destino");
			ciuDest = String.format(cdciuDest);
			return ciuDest;
		}

		static String capdir() {
			String dir;
			String cddir = FormInput.leeString("Direccion");
			dir = String.format(cddir);
			return dir;
		}

		static LocalDate capfechaViaje() {
			LocalDate fechaViaje;
			String fvfechaViaje = FormInput.leeString("Ingresa por favor la  fecha del Viaje: [aaaa-mm-dd]:");
			fechaViaje = LocalDate.parse(fvfechaViaje);
			return fechaViaje;
		}

		static LocalDate capfechaReg() {
			LocalDate fechaReg;
			String frfechaReg = FormInput.leeString("Ingresa por favor la fecha de Regreso: [aaaa-mm-dd]:");
			fechaReg = LocalDate.parse(frfechaReg);
			return fechaReg;
		}

		static String capdescripCarga() {
			String descripCarga;
			String dcdescripCarga = FormInput.leeString("Ingresa por favor la descripcion de la carga");
			descripCarga = String.format(dcdescripCarga);
			return descripCarga;
		}

		static String capmontViaje() {
			String montViaje;
			String mvmontViaje = FormInput.leeString("Ingresa por favor el Monto del viaje");
			montViaje = String.format(mvmontViaje);
			return montViaje;
		}
		/***************************/
		static Viajes Viajes() {
			String ciuDest = capciuDest();
			String dir = capdir();
			LocalDate FechaViaje = capfechaViaje();
			LocalDate FechaReg = capfechaReg();
			String descripCarga = capdescripCarga();
			String montViaje = capmontViaje();
			return new Viajes(ciuDest, dir, FechaViaje, FechaReg, descripCarga, montViaje);
		}
		/***************************/
		@SuppressWarnings("unlikely-arg-type")
		static void addAsignarViaje() {
			Viajes viaje = Viajes();
			viajes.add(viaje);
			for (Viajes viajes : viajes)
				if (FormInput.leeBoolean(vehiculos + "\nDesea escoger este u otro vehiculo para su viaje:")) {
					vehiculos.contains(viajes);
				}
		}

		static void CancelarViaje() {
			for (Viajes viaje : viajes)
				if (FormInput.leeBoolean(viaje + "\nEste el viaje a eliminar:")) {
					viajes.remove(viaje);
					break;
				}
		}
		
		static void ViajesAsignados() {
			FormOutput.imprimeListaTabla(viajes, "Viajes asignados");
			FormOutput.imprimeListaTabla(vehiculos, "Vehiculos");			
		}
		/***************************/
		public static void run() {
			generaMenu();
			int opc;
			do {
				opc = menu.menuOption();
				switch (opc) {
				case 1:
					addVehiculo();
					break;
				case 2:
					listVehiculos();
					break;
				case 3:
					EliminarVehiculo();
					break;
				case 4:
					CancelarViaje();
					break;
				case 5:
					addAsignarViaje();
					break;
				case 6:
					ViajesAsignados();
					break;
				}
			} while (opc != FIN);
		}
	}
	


